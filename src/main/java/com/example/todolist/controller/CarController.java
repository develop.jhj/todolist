package com.example.todolist.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.todolist.model.Car;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class CarController {
	
    @GetMapping("/cars")
    public List<Car> getCars() {
    	List<Car> carList = new ArrayList<>();
        carList.add(new Car("Tesla", 2021));
        carList.add(new Car("Hyundai", 2020));
        carList.add(new Car("Toyota", 2019));
        return carList;
    }

    @GetMapping("/cars/afterYear/{year}")
    public List<Car> getCarsAfterYear(@PathVariable("year") int year) {
    	List<Car> carList = new ArrayList<>();
        carList.add(new Car("Tesla", 2021));
        carList.add(new Car("Hyundai", 2020));
        carList.add(new Car("Toyota", 2019));
        
        return carList.stream()
                .filter(car -> car.getYear() >= year)
                .collect(Collectors.toList());
    }
    
    @GetMapping("/cars/{index}")
    public Car getCarByIndex(@PathVariable("index") int index) throws Exception {
    	List<Car> carList = new ArrayList<>();
        carList.add(new Car("Tesla", 2021));
        carList.add(new Car("Hyundai", 2020));
        carList.add(new Car("Toyota", 2019));
        
        try {
        	return carList.get(index);
        }catch (Exception e) {
			// TODO: handle exception
        	throw new Exception("에러");
		}
//    	if(index >= 0 && index < carList.size()) {
//        	return carList.get(index);
//        }else {
//        	throw new Exception("에러");
//        }
    }
}

