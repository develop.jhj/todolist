package com.example.todolist.controller;

import com.example.todolist.model.TodoItem;
import com.example.todolist.service.TodoItemService;
import com.example.todolist.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/todoitems")
public class TodoItemController {

    private final TodoItemService todoItemService;

    @Autowired
    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @GetMapping
    public String getAllTodoItems(Model model) {
    	List<TodoItem> todos = todoItemService.findAll();

        List<Map<String, Object>> todosWithDaysRemaining = todos.stream().map(todo -> {
            Map<String, Object> todoMap = new HashMap<>();
            todoMap.put("id", todo.getId());
            todoMap.put("task", todo.getTask());
            todoMap.put("dueDate", todo.getDueDate());
            todoMap.put("status", todo.getStatus());
            todoMap.put("priority", todo.getPriority());
            todoMap.put("daysRemaining", DateUtil.calculateDaysRemaining(todo.getDueDate()));
            return todoMap;
        }).collect(Collectors.toList());
        
        model.addAttribute("todos", todosWithDaysRemaining);
    	
        return "todos";
    }

    @GetMapping("/{id}")
    public TodoItem getTodoItemById(@PathVariable("id") Long id) {
        return todoItemService.findById(id);
    }

    @PostMapping
    public ResponseEntity<?> addTodoItem(@RequestBody TodoItem todoItem) {
    	TodoItem insTodoItem = todoItemService.save(todoItem);
        return ResponseEntity.ok(todoItem);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<TodoItem> updateTodoItem(@PathVariable("id") Long id, @RequestBody TodoItem updatedTodoItem) {
    	TodoItem updateTodoItem = todoItemService.update(id, updatedTodoItem);
        return ResponseEntity.ok(updateTodoItem);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTodoItem(@PathVariable("id") Long id) {
        todoItemService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

