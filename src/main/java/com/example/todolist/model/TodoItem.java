package com.example.todolist.model;

import lombok.Data;

import java.time.LocalDate;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@DynamicUpdate
@Table(name = "todo_list")
@Data
public class TodoItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String task;//할일
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;//마감일
    private String status;//할 일의 상태(예: '1:완료', '2:진행 중')
    private Integer priority;//우선순위
}
