package com.example.todolist.service;

import com.example.todolist.model.TodoItem;
import com.example.todolist.mapper.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoItemService {

    private final TodoItemRepository todoItemRepository;

    @Autowired
    public TodoItemService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    public List<TodoItem> findAll() {
        return todoItemRepository.findAll();
    }

    public TodoItem findById(Long id) {
        return todoItemRepository.findById(id)
        		.orElseThrow(() -> new RuntimeException("todo not found with id " + id));
    }

    public TodoItem save(TodoItem todoItem) {
        return todoItemRepository.save(todoItem);
    }
    
    public TodoItem update(Long id, TodoItem updatedTodoItem) {
        TodoItem existingTodoItem = todoItemRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("TodoItem not found with id: " + id));
        
        if (updatedTodoItem.getTask() != null) existingTodoItem.setTask(updatedTodoItem.getTask());
        if (updatedTodoItem.getDueDate() != null) existingTodoItem.setDueDate(updatedTodoItem.getDueDate());
        if (updatedTodoItem.getStatus() != null) existingTodoItem.setStatus(updatedTodoItem.getStatus());
        if (updatedTodoItem.getPriority() != null) existingTodoItem.setPriority(updatedTodoItem.getPriority());
        
        return todoItemRepository.save(existingTodoItem);
    }

    public void delete(Long id) {
        todoItemRepository.deleteById(id);
    }
}

