package com.example.todolist.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateUtil {

    public static String formatDate(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dateTime.format(formatter);
    }
    
    public static long calculateDaysRemaining(LocalDate dueDate) {
        LocalDate today = LocalDate.now();
        return ChronoUnit.DAYS.between(today, dueDate);
    }
    
}